import React, { Component } from 'react';
import { Col, Layout, Row } from 'antd';

import './style.scss';
import Header from '../../components/Header';
import Chart from '../../components/Chart';
import MenuProject from '../../components/MenuProject';
import { getData } from '../../api/GetData';
import ActiveUser from '../../components/Chart/ActiveUser';
import ColumnCharts from '../../components/ColumnChart';
import SelectedPeriod from '../../components/SelectedPeriod';
import TotalInvertment from '../../components/TotalInvertment';

const ran = (max) => Math.floor(Math.random() * Math.floor(max));
class Dashboard extends Component {
    state = {
        week: [],
        month: [],
        day: [],
        respond: [],
        item: null,
        listActiveUser: [],
    };

    componentDidMount() {
        getData((data) => {
            this.setState({
                week: data.week,
                day: data.date,
                month: data.month,
                respond: data.data,
                listActiveUser: [data.data[0], data.data[1], data.data[2]],
            });
        });
    }

    randomUser = () => {
        const { respond } = this.state;
        let number = ran(respond.length);
        let newArray = [];

        for (let i = number; i < number + 3; i++) {
            newArray.push(respond[i]);
        }
        this.setState({ listActiveUser: newArray });
    };

    render() {
        const { week, month, day, listActiveUser } = this.state;
        return (
            <div className="container">
                <Row style={{ padding: 0, width: '100%' }}>
                    <Col xs={0} sm={0} xl={2}>
                        <MenuProject />
                    </Col>
                    <Col xs={24} sm={24} xl={22}>
                        <Header />
                        <Row gutter={{ lg: 32 }} style={{ padding: 0, width: '100%' }}>
                            <Col
                                xs={24}
                                sm={24}
                                lg={14}
                                style={{ marginTop: 32 }}
                                className="wrapChart"
                            >
                                <Chart day={day} week={week} month={month} />
                            </Col>
                            <Col xs={24} sm={24} lg={10} style={{ marginTop: 32 }}>
                                <ActiveUser
                                    day={day}
                                    listActiveUser={listActiveUser}
                                    randomUser={this.randomUser}
                                />
                            </Col>
                        </Row>
                        <Row
                            gutter={{ lg: 32 }}
                            style={{
                                padding: 0,
                                width: '100%',
                                alignItems: 'flex-end',
                                marginBottom: '32px',
                            }}
                        >
                            <Col xs={24} sm={24} lg={8} xxl={8} className="margin-32">
                                <SelectedPeriod />
                            </Col>
                            <Col xs={24} sm={24} lg={6} xxl={6} className="margin-32">
                                <TotalInvertment />
                            </Col>
                            <Col xs={24} sm={24} lg={10} xxl={10} className="margin-32">
                                <ColumnCharts />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Dashboard;
