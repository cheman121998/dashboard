import React from 'react';
import PropTypes from 'prop-types';

import { Button, Input, message } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import './search.scss';

const Search = ({}) => {
    return (
        <Input
            placeholder="Search"
            className="searchBox"
            suffix={
                <Button
                    type="text"
                    icon={<SearchOutlined className="searchIcon" />}
                    onClick={() => message.success('Search')}
                />
            }
        />
    );
};

Search.propTypes = {};

export default Search;
