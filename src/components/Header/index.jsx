import React, { Component } from 'react';
import style from './style.scss';

import Search from '../InputSearch/Search';
import { Button, Drawer, Dropdown, Input, Menu, message, Tooltip } from 'antd';
import {
    DesktopOutlined,
    ContainerOutlined,
    ShopOutlined,
    SettingOutlined,
    BarChartOutlined,
    InboxOutlined,
    PlusOutlined,
    MenuFoldOutlined,
    SearchOutlined,
} from '@ant-design/icons';

const menuDrop = (
    <Menu style={{ backgroundColor: '#9f9bd1' }}>
        <Menu.Item>Theme</Menu.Item>
        <Menu.Item>Language</Menu.Item>
    </Menu>
);

const iconStyle = { fontSize: 20, color: ' #635e98' };
export default class Header extends Component {
    state = {
        showMenu: false,
    };
    render() {
        return (
            // <div className="container">
            <header>
                <h1>DASHBOARD</h1>
                <div className="wrapMenu">
                    <Button
                        shape="circle"
                        icon={<PlusOutlined style={{ color: ' #6d46ff' }} />}
                        className="buttonPlus"
                        onClick={() => message.success('Plus')}
                    />
                    <h3>ADD FUNDS</h3>

                    <Search />

                    <Dropdown trigger="click" overlay={menuDrop}>
                        <Tooltip title="Setting">
                            <Button type="text" icon={<SettingOutlined style={iconStyle} />} />
                        </Tooltip>
                    </Dropdown>
                </div>
                <div className="showInMobile">
                    <button onClick={() => this.setState({ showMenu: !this.state.showMenu })}>
                        <MenuFoldOutlined style={iconStyle} />
                    </button>

                    <Drawer
                        placement={'right'}
                        closable={true}
                        visible={this.state.showMenu}
                        bodyStyle={{ backgroundColor: '#001529' }}
                        onClose={() => this.setState({ showMenu: false })}
                    >
                        <Menu
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            mode="inline"
                            theme="dark"
                            onClick={() => this.setState({ showMenu: false })}
                            className="menuAnt"
                        >
                            <Menu.Item key="1" icon={<BarChartOutlined />}>
                                Chart
                            </Menu.Item>
                            <Menu.Item key="2" icon={<DesktopOutlined />}>
                                Desktop
                            </Menu.Item>
                            <Menu.Item key="3" icon={<ShopOutlined />}>
                                Shop
                            </Menu.Item>
                            <Menu.Item key="4" icon={<InboxOutlined />}>
                                Contact
                            </Menu.Item>
                            <Menu.Item key="5" icon={<ContainerOutlined />}>
                                Container
                            </Menu.Item>
                            <Menu.Item key="6" icon={<SettingOutlined />}>
                                Setting
                            </Menu.Item>
                            <Menu.Item key="7" icon={<PlusOutlined />}>
                                ADD FUNDS
                            </Menu.Item>
                            <Search />
                        </Menu>
                    </Drawer>
                </div>
            </header>
            // </div>
        );
    }
}
