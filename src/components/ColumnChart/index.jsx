import React, { Component, createRef } from 'react';
import { Col, DatePicker, Space, Row, Select } from 'antd';
import './style.scss';
import { Bar } from '@ant-design/charts';
import { getData, getDataTopCity } from '../../api/GetData';
import { dataTopCity } from '../../api/MockDataTopCity';

const ran = (max) => Math.floor(Math.random() * Math.floor(max));

const month = [
    { key: '1', label: 'January 2019' },
    { key: '2', label: 'February 2019' },
    { key: '3', label: 'March 2019' },
    { key: '4', label: 'April 2019' },
    { key: '5', label: 'May 2019' },
    { key: '6', label: 'June 2019' },
    { key: '7', label: 'January 2019' },
    { key: '8', label: 'August 2019' },
];
function onChange(date, dateString) {
    console.log(date, dateString);
}

export default class ColumnCharts extends React.Component {
    state = {
        listTopCity: [],
    };

    componentDidMount() {
        this.randomCity();
    }

    randomCity = () => {
        let number = ran(dataTopCity.length - 4);
        let newArray = [];

        for (let i = number; i < number + 4; i++) {
            newArray.push(dataTopCity[i]);
        }

        this.setState({ listTopCity: newArray });
    };

    render() {
        const config = {
            forceFit: true,
            data: this.state.listTopCity,
            xField: 'deal',
            yField: 'country',
            conversionTag: { visible: false },
        };
        return (
            <div className="column-chart">
                <Row>
                    <Col xs={24} md={24} lg={24} className="top-city">
                        <div className="top-city-item">
                            <div>Top cities</div>
                            <div>
                                <Select
                                    defaultValue="8"
                                    dropdownStyle={{
                                        backgroundColor: '#9f9bd1',
                                    }}
                                    onChange={this.randomCity}
                                >
                                    {month.map((e) => (
                                        <Select.Option key={e.key}>{e.label} </Select.Option>
                                    ))}
                                </Select>
                            </div>
                        </div>
                        <Bar {...config} />
                    </Col>
                </Row>
            </div>
        );
    }
}
