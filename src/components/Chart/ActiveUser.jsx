import { Menu, Row, Select, Col, Dropdown, Slider, Divider } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import React, { Component } from 'react';
import BoxContent from '../BoxContent';
import Loading from '../Loading/Loading';
import './style.scss';

const month = [
    { key: '1', label: 'January 2019' },
    { key: '2', label: 'February 2019' },
    { key: '3', label: 'March 2019' },
    { key: '4', label: 'April 2019' },
    { key: '5', label: 'May 2019' },
    { key: '6', label: 'June 2019' },
    { key: '7', label: 'January 2019' },
    { key: '8', label: 'August 2019' },
];
const menuUser = (
    <Menu style={{ backgroundColor: '#9f9bd1', width: 150 }}>
        <Menu.Item key="Follow">Follow</Menu.Item>
        <Menu.Item key="Chat">Chat</Menu.Item>
        <Menu.Item key="Hidden">Hidden</Menu.Item>
    </Menu>
);

const WrapUser = ({ name, avatar, address, level, point, slider }) => (
    <div className="user">
        <Row justify="space-between">
            <Col style={{ display: 'flex', alignItems: 'center' }}>
                <Avatar src={avatar} shape="circle" style={{ width: 40, height: 40 }} />
                <div>
                    <h2 style={{ margin: 'auto 10px' }}>{name} </h2>
                    <h3 style={{ margin: '0 10px' }}>{address}</h3>
                </div>
            </Col>
            <Col>
                <Dropdown.Button
                    trigger="click"
                    overlay={menuUser}
                    icon={<ion-icon name="ellipsis-horizontal-outline" />}
                ></Dropdown.Button>
            </Col>
        </Row>
        <Slider value={slider} disabled={true} />
        <Row justify="space-between">
            <h4>Professional Level {level}</h4>
            <h4 style={{ fontWeight: 'bold' }}>{point} Points</h4>
        </Row>
        <Divider style={{ borderColor: '#363071', margin: 10 }} />
    </div>
);
export default class ActiveUser extends Component {
    render() {
        const { day, listActiveUser } = this.props;
        return (
            <BoxContent>
                <Row justify="space-between" style={{ marginBottom: '30px' }}>
                    <h2>Active Users</h2>
                    <Select
                        defaultValue="8"
                        dropdownStyle={{ backgroundColor: '#9f9bd1' }}
                        onChange={this.props.randomUser}
                    >
                        {month.map((e) => (
                            <Select.Option key={e.key}>{e.label} </Select.Option>
                        ))}
                    </Select>
                </Row>

                {day.length > 0 ? (
                    listActiveUser.map((e, i) => (
                        <WrapUser
                            key={i}
                            name={e.full_name}
                            level={e.level}
                            avatar={e.avatar}
                            address={`${e.city}, ${e.country}`}
                            point={e.deal}
                            slider={e.slider}
                        />
                    ))
                ) : (
                    <Loading />
                )}
            </BoxContent>
        );
    }
}
