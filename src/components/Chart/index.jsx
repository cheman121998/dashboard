import React, { Component, createRef } from 'react';
import { Row, Col, Select, Avatar, Slider, Divider, Dropdown, Menu } from 'antd';
import { Area, Line } from '@ant-design/charts';

import { getData } from '../../api/GetData';
import './style.scss';
import { avtOnce, avtTow, avtThree } from '../../assets/avatar';
import Loading from '../Loading/Loading';
import LineCharts from '../LineChart';

export default class ChartX extends Component {
    state = { select: '1' };

    clickButton = (button) => this.setState({ select: button });
    buttonComponents = ({ key, label }) => (
        <button
            type="button"
            onClick={() => this.clickButton(key)}
            className={this.state.select == key ? 'focus' : null}
        >
            {label}
        </button>
    );

    config = () => {
        //config change with buttonComponents
        const { day, week, month } = this.props;
        const { select } = this.state;
        if (select === '1') {
            return {
                data: day,
                xField: 'time',
                yField: 'total',
                xAxis: {
                    type: 'dateTime',
                    tickCount: 5,
                },
            };
        }
        if (select === '2') {
            return { data: week, xField: 'date', yField: 'total' };
        }
        return { data: month, xField: 'date', yField: 'total' };
    };
    render() {
        return (
            <div className="chartContainer">
                <div className="wrapButton">
                    {this.buttonComponents({ key: '1', label: 'DAY' })}
                    {this.buttonComponents({ key: '2', label: 'WEEK' })}
                    {this.buttonComponents({ key: '3', label: 'MONTH' })}
                    {this.buttonComponents({ key: '4', label: 'YEAR' })}
                    {this.buttonComponents({ key: '5', label: 'ALL' })}
                </div>
                <div style={{ height: 30 }} />
                {this.props.day.length > 0 ? <Area {...this.config()} /> : <Loading />}
            </div>
        );
    }
}
