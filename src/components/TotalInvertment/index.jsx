import React, { Component, useState, useEffect } from 'react';
import { DashOutlined } from '@ant-design/icons';
import './style.scss';
import { Donut } from '@ant-design/charts';
import { Slider, Row, Col, Menu, Dropdown } from 'antd';

const marks = {
    100: {
        style: {
            color: '#928DC4',
        },
        label: <strong>8653.59</strong>,
    },
    50: {
        style: {
            color: '#928DC4',
        },
        label: <strong style={{ fontWeight: 'unset' }}>PERIOD RANGE</strong>,
    },
    0: {
        style: {
            color: '#928DC4',
        },
        label: <strong>3534.68</strong>,
    },
};

const menuUser = (
    <Menu style={{ backgroundColor: '#9f9bd1', width: 150 }}>
        <Menu.Item key="Follow">See more</Menu.Item>
        <Menu.Item key="Hidden">Hidden</Menu.Item>
    </Menu>
);
const data = [
    {
        type: 'EUR/USD    $1,3345',
        value: 15,
    },
    {
        type: 'ZEC/EUR     $123,334',
        value: 10,
    },
    {
        type: 'XBT/EUR     $6,5567',
        value: 5,
    },
];
const config = {
    forceFit: true,

    description: {
        visible: true,
    },
    radius: 0.8,
    padding: 'auto',
    margin: '20px',
    data,
    angleField: 'value',
    colorField: 'type',
    statistic: { visible: true },
};

export default class TotalInvertment extends Component {
    state = {
        reverse: true,
    };

    handleReverseChange = (reverse) => {
        this.setState({ reverse });
    };
    render() {
        const { reverse } = this.state;
        return (
            <div className="total-invertment">
                <div className="total-title">
                    <h2>Total Investment</h2>
                    <Dropdown.Button
                        trigger="click"
                        overlay={menuUser}
                        icon={<ion-icon name="ellipsis-horizontal-outline" />}
                    ></Dropdown.Button>
                </div>
                <Donut {...config} />
            </div>
        );
    }
}
