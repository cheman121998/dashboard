import React, { Component } from 'react';
import './box.scss';

class BoxContent extends Component {
    render() {
        return <div className="boxContent">{this.props.children}</div>;
    }
}

export default BoxContent;
