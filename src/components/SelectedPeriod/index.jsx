import React, { Component, useState, useEffect } from 'react';
import { DashOutlined } from '@ant-design/icons';
import './style.scss';

import { Slider, Row, Col, Menu, Dropdown } from 'antd';

const marks = {
    100: {
        style: {
            color: '#928DC4',
        },
        label: <strong>8653.59</strong>,
    },
    50: {
        style: {
            color: '#928DC4',
        },
        label: <strong style={{ fontWeight: 'unset' }}>PERIOD RANGE</strong>,
    },
    0: {
        style: {
            color: '#928DC4',
        },
        label: <strong>3534.68</strong>,
    },
};

const menuUser = (
    <Menu style={{ backgroundColor: '#9f9bd1', width: 150 }}>
        <Menu.Item key="Follow">See more</Menu.Item>
        <Menu.Item key="Hidden">Hidden</Menu.Item>
    </Menu>
);

const data = [
    {
        type: 'EUR/USD    $1,3345',
        value: 15,
    },
    {
        type: 'ZEC/EUR     $123,334',
        value: 10,
    },
    {
        type: 'XBT/EUR     $6,5567',
        value: 5,
    },
];
const config = {
    forceFit: true,

    description: {
        visible: true,
    },
    radius: 0.8,
    padding: 'auto',
    margin: '20px',
    data,
    angleField: 'value',
    colorField: 'type',
    statistic: { visible: true },
};
export default class SelectedPeriod extends Component {
    state = {
        reverse: true,
    };

    handleReverseChange = (reverse) => {
        this.setState({ reverse });
    };
    render() {
        const { reverse } = this.state;
        return (
            <div className="select-period">
                <div className="select-title">
                    <div className="select-title-sub">
                        <h2>Selected Period</h2>
                        <Dropdown.Button
                            trigger="click"
                            overlay={menuUser}
                            icon={<ion-icon name="ellipsis-horizontal-outline" />}
                        ></Dropdown.Button>
                    </div>
                </div>
                <div className="wrap-content">
                    <h2>$6587.56</h2>
                    <h4>STARTING PRICE</h4>
                </div>

                <div className="wrap-content">
                    <h2>$2.936,99 (-5%)</h2>
                    <h4>PERIOD CHANGE</h4>
                </div>
                <Slider reverse={reverse} range marks={marks} defaultValue={[20, 50]} />
            </div>
        );
    }
}
